import json

import allure
import curlify
import requests


class ApiClient:
    def __init__(self, base_address):
        self.base_address = base_address

    def _prepare_request(self, method, url, headers, data=None, json=None, params=None) -> requests.PreparedRequest:
        req = requests.Request(method=method, url=url, headers=headers, data=data, json=json, params=params)
        return req.prepare()

    def _send_request(self, r: requests.PreparedRequest) -> requests.Response:
        save_request(r)

        response = requests.session().send(r)

        save_response(response)
        return response

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        prepared_req = self._prepare_request(
            method="post",
            url=f"{self.base_address}{path}",
            headers=headers,
            data=data,
            json=json,
            params=params,
        )
        with allure.step(f"POST request to: {prepared_req.url}"):
            return self._send_request(prepared_req)

    def get(self, path="/", params=None, headers=None):
        prepared_req = self._prepare_request(
            method="get",
            url=f"{self.base_address}{path}",
            headers=headers,
            params=params,
        )
        with allure.step(f"GET request to: {prepared_req.url}"):
            return self._send_request(prepared_req)


def save_request(r: requests.PreparedRequest):
    curl = curlify.to_curl(r)

    allure.attach(body=curl, name="request", attachment_type=allure.attachment_type.TEXT)


def save_response(r: requests.Response):
    json_text = json.dumps(r.json(), indent=4)

    allure.attach(body=json_text, name="response", attachment_type=allure.attachment_type.JSON)
