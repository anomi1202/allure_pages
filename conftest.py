import pytest

from api_client import ApiClient


@pytest.fixture
def dog_api():
    return ApiClient(base_address="https://dog.ceo/api/")
