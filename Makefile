.PHONY: test
test:
	rm -rf ./allure-results
	pytest -n=4 --alluredir=./allure-results tests/


.PHONY: black
black:
	black . -l 120